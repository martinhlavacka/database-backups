<?php 

	/*

		CREDIT
		---------------------------------------
		Author: 	Martin Hlavacka
		Contact: 	martinhlavacka@outlook.com 
		Date: 		06.04.2018
				
		LICENSE
		---------------------------------------
		Permission is hereby granted, free of charge, to any person obtaining a copy
		of this software and associated documentation files (the "Software"), to deal
		in the Software without restriction, including without limitation the rights
		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
		copies of the Software, and to permit persons to whom the Software is
		furnished to do so, subject to the following conditions:

		The above copyright notice and this permission notice shall be included in all
		copies or substantial portions of the Software.

		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
		SOFTWARE.

		USAGE
		---------------------------------------
		1. Create folder called "backups"
		2. Update host, username, password and database values

	*/

	// CREATE NEW OBJECT
	$databaseBackup = new DatabaseBackup("DATABASE-HOST", "DATABASE-USERNAME", "DATABASE-PASSWORD", "DATABASE-NAME");
	
	// CLASS DEFINITION
	class DatabaseBackup{

		// DATABASE PROPERTIES
		private $database_host;
		private $database_username;
		private $database_password;
		private $database_name;

		// BACKUP PROPERTIES
		private $backup_name;
		private $backup_location;

		// CONSTRUCTOR
		public function __construct($host, $username, $password, $database){

			// DATABASE DETAILS
			$this->database_host 		= $host;
			$this->database_username 	= $username;
			$this->database_password	= $password;
			$this->database_name		= $database;

			// BACKUP DETAILS
			$this->backup_name 			= $database . "-" . date("d-m-Y-H-i-s");
			$this->backup_location 		= "backups/" . $this->backup_name . ".sql";

			// REQUIRE BACKUP LIBRARY
			require_once("library/library.php");

			// EXECUTE BACKUP
			$this->backupNow($this);

		}

		// BACKUP METHOD
		public function backupNow($database){

			// TRY TO BACKUP DATA
			try{

				// CONNECT TO DATABASE
				$dump = new Ifsnop\Mysqldump\Mysqldump('mysql:host=' . $database->database_host . ';dbname=' . $database->database_name . '', $database->database_username, $database->database_password);

				// BACKUP DATABASE
				$dump->start($database->backup_location);

				// VERIFY RESULT
				$this->verifyBackup($database->backup_location);

			}

			// IF BACKUP FAILES
			catch(Exception $exception){

				// DISPLAY ERROR MESSAGE
				echo $exception;

			}

		}

		// VERIFICATION METHOD
		public function verifyBackup($backup_file){

			// IF BACKUP WAS CREATED
			if (file_exists($backup_file)){
				echo "Database backup was succesully created";
			}

			// IF CREATION OF THE BACKUP FAILED
			else{
				echo "Creation of the database backup failed";
			}

		}

	}

?>